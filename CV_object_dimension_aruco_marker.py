from scipy.spatial import distance as dist
import numpy as np
import imutils
from imutils import contours
from imutils import perspective
import cv2

from object_detector import *
detector = detectorObj()

def get_aruco_dict(image):
    ARUCO_DICT = {
    "DICT_4X4_50": cv2.aruco.DICT_4X4_50,
    "DICT_4X4_100": cv2.aruco.DICT_4X4_100,
    "DICT_4X4_250": cv2.aruco.DICT_4X4_250,
    "DICT_4X4_1000": cv2.aruco.DICT_4X4_1000,
    "DICT_5X5_50": cv2.aruco.DICT_5X5_50,
    "DICT_5X5_100": cv2.aruco.DICT_5X5_100,
    "DICT_5X5_250": cv2.aruco.DICT_5X5_250,
    "DICT_5X5_1000": cv2.aruco.DICT_5X5_1000,
    "DICT_6X6_50": cv2.aruco.DICT_6X6_50,
    "DICT_6X6_100": cv2.aruco.DICT_6X6_100,
    "DICT_6X6_250": cv2.aruco.DICT_6X6_250,
    "DICT_6X6_1000": cv2.aruco.DICT_6X6_1000,
    "DICT_7X7_50": cv2.aruco.DICT_7X7_50,
    "DICT_7X7_100": cv2.aruco.DICT_7X7_100,
    "DICT_7X7_250": cv2.aruco.DICT_7X7_250,
    "DICT_7X7_1000": cv2.aruco.DICT_7X7_1000,
    "DICT_ARUCO_ORIGINAL": cv2.aruco.DICT_ARUCO_ORIGINAL,
    "DICT_APRILTAG_16h5": cv2.aruco.DICT_APRILTAG_16h5,
    "DICT_APRILTAG_25h9": cv2.aruco.DICT_APRILTAG_25h9,
    "DICT_APRILTAG_36h10": cv2.aruco.DICT_APRILTAG_36h10,
    "DICT_APRILTAG_36h11": cv2.aruco.DICT_APRILTAG_36h11
}
    # loop dict 
    support_dict = []
    for (arucoName, arucoDict) in ARUCO_DICT.items():
        # setup the dict
        arucoDict = cv2.aruco.Dictionary_get(arucoDict)
        arucoParams = cv2.aruco.DetectorParameters_create()
        (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict, parameters=arucoParams)
        # check the corners detected or not
        if len(corners) > 0:
            support_dict.append(arucoName)
            
    return support_dict

# load image and get contour details
def get_contour(image):
    # resize image
    # image = imutils.resize(image, width=500)

    # convert image to gray 
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # apply gaussian blur
    gray = cv2.GaussianBlur(gray, (7, 7), 0)

    # apply canny edge detection
    edged = cv2.Canny(gray, 50, 100)

    # apply dilation - fill the small gaps
    edged = cv2.dilate(edged, None, iterations=1)

    # apply erosion - remove unwanted pixels
    edged = cv2.erode(edged, None, iterations=1)

    # find contours
    img_contour = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    img_contour = imutils.grab_contours(img_contour)

    # sort the contours
    (img_contour, _) = contours.sort_contours(img_contour)

    return img_contour

# detect aruco marker

def findArucoMarkers(img, markerSize = 5, totalMarkers=100, draw=True):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    key = getattr(cv2.aruco, f'DICT_{markerSize}X{markerSize}_{totalMarkers}')
    
    arucoDict = cv2.aruco.Dictionary_get(key)
    arucoParam = cv2.aruco.DetectorParameters_create()
    
    # detect markers
    contour, ids, rejected = cv2.aruco.detectMarkers(gray, arucoDict, parameters = arucoParam)

    # to check the aruco dict match for the input image
    # check for the image if not fit
    support_dict = get_aruco_dict(image)

    return contour, ids, rejected


def get_pixel_details(image):
    # Detect Aruco marker and use 
    #it's dimension to calculate the pixel to inch ratio
    arucofound =findArucoMarkers(image, totalMarkers=100)

    
    if  len(arucofound[0])!=0:
        print(arucofound[0][0][0])
        aruco_perimeter = cv2.arcLength(arucofound[0][0][0], True)
        print(aruco_perimeter)
        # Pixel to Inch ratio
        # 20 because used 5x5 marker, 5+5+5+5
        pixelsPerMetric = aruco_perimeter / 20 
        print(" pixel to inch",pixelsPerMetric)

    else:
        # based on the multiple image calculation
        pixelsPerMetric=106.60

    return pixelsPerMetric

img_path = r"./logo_samples.jpg"

image=cv2.imread(img_path)
img_contour = get_contour(image)

# for pixel to inch calibration 
pixelsPerMetric = get_pixel_details(image)

def midpoint(ptA, ptB):
    return ((ptA[0] + ptB[0]) / 2 , (ptA[1] + ptB[1]) / 2)

# pixel calculation is not correct 
pixel_per_inch = None



# loop over the contours individually
for c in img_contour:
    
    # if the contour is not sufficiently large, ignore it
    if cv2.contourArea(c) < 2000:
        continue
   
    # compute the rotated bounding box of the contour
    box = cv2.minAreaRect(c)
    (x, y), (w, h), angle = box
    box = cv2.boxPoints(box)
    box = np.int0(box)
    cv2.drawContours(image,[box],0,(0,0,255),2)

    
    (x1, x2, y2, y1) = box
    (tltrX, tltrY) = midpoint(x1, x2)
    (blbrX, blbrY) = midpoint(y1, y2)
    (tlblX, tlblY) = midpoint(x1, y1)
    (trbrX, trbrY) = midpoint(x2, y2)
    dist_A = dist.euclidean((tltrX, tltrY), (blbrX, blbrY))
    dist_B = dist.euclidean((tlblX, tlblY), (trbrX, trbrY))

    if pixel_per_inch is None:
        # 106.60 calculated based on the screen size
        pixel_per_inch = dist_B / (dist_B/106.60)
   
    # convert pixel to inches
    dimA = dist_A / pixel_per_inch
    dimB = dist_B / pixel_per_inch

    # value for 2.54 inches to centimeter 
    dimA = dimA * 2.54
    dimB = dimB * 2.54



    cv2.putText(image, "{:.1f}cm".format(dimA), (int(tltrX - 15), int(tltrY - 10)), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
    cv2.putText(image, "{:.1f}cm".format(dimB), (int(trbrX + 10), int(trbrY)), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)

    cv2.imshow("Image", image)
    cv2.waitKey(0)