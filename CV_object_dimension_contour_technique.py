# identify the objects using contour technique
# measure the dimension of the object using euclidean distance
# convert inches to cm
# measure the object size using cm scale (manual scale and using ruler both same value)
# note: camera callibration method not used

#Packages: scipy, imutils, numpy, opencv-python
# simple cv technique - refered github repos
# contours technique used during table detection technqiue in scanned forms 
# including horizontal and vertical line detection.

from scipy.spatial import distance as dist
from imutils import perspective
from imutils import contours
import numpy as np
import imutils
import cv2


img_path = r"./logo_samples.jpg"

def midpoint(ptA, ptB):
    return ((ptA[0] + ptB[0]) / 2 , (ptA[1] + ptB[1]) / 2)


pixel_per_inch = None
# read image and convert to gray
image = cv2.imread(img_path)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = cv2.GaussianBlur(gray, (7, 7), 0)

# apply edge detection technique
edged = cv2.Canny(gray, 50, 100)
edged = cv2.dilate(edged, None, iterations=1)
edged = cv2.erode(edged, None, iterations=1)

# find the contours and sort 
img_contour = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
img_contour = imutils.grab_contours(img_contour)
(img_contour, _) = contours.sort_contours(img_contour)

orig = image.copy()
for _contour in img_contour:
    # ignore if the size is very less
    if cv2.contourArea(_contour) < 500:
        continue

    # convert contour into areas

    box = cv2.minAreaRect(_contour)
    box = cv2.cv.BoxPoints(box) if imutils.is_cv2() else cv2.boxPoints(box)
    box = np.array(box, dtype="int")
    box = perspective.order_points(box)
    cv2.drawContours(orig, [box.astype("int")], -1, (95, 255, 95), 2)

    (x1, x2, y2, y1) = box
    (tltrX, tltrY) = midpoint(x1, x2)
    (blbrX, blbrY) = midpoint(y1, y2)
    (tlblX, tlblY) = midpoint(x1, y1)
    (trbrX, trbrY) = midpoint(x2, y2)


    dist_A = dist.euclidean((tltrX, tltrY), (blbrX, blbrY))
    dist_B = dist.euclidean((tlblX, tlblY), (trbrX, trbrY))

    if pixel_per_inch is None:
        # 106.60 calculated based on the screen size
        pixel_per_inch = dist_B / (dist_B/106.60)
   
    # convert pixel to inches
    dimA = dist_A / pixel_per_inch
    dimB = dist_B / pixel_per_inch

    # value for 2.54 inches to centimeter 
    dimA = dimA * 2.54
    dimB = dimB * 2.54

    cv2.putText(orig, "{:.1f}cm".format(dimA), (int(tltrX - 15), int(tltrY - 10)), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
    cv2.putText(orig, "{:.1f}cm".format(dimB), (int(trbrX + 10), int(trbrY)), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)

cv2.imshow("Image", orig)
cv2.waitKey(0)